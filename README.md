# README #

Android Plugin for Unity that enables indoor location tracking using IndoorAtlas SDK. 

### What is this repository for? ###

* This library enables execution of native java code in Unity to enable Indoor Location Tracking using IndoorAtlas SDK
* Version - 0.1

### How do I get set up? ###

* Summary of set up
1.Install the plugins 
> Copy *finallocationlibrary-release* and *indooratlas-android-sdk-2.5.3* to  Assests > Plugins > Android in Unity
> > Click on the plugin and make sure the android checkbox is ticked. 

* Configuration
### Function Calls ###
> #### testLibarary(): void  
>*	Logs ("UnityLocation: Function Called, Object Exists(Bridge Words)") to Android ADB console 
>Call from unity
>	AndroidJavaObject

> #### setContext(Context UnityPlayerContext): void 

> #### startLocationServices(): void 

> #### pauseLocationServices(): void

> #### stopLocationServices(): void

> #### resumeLocationServices(): void

> #### changeDefaultFloorPlanID(string FloorPlanID*): void

> #### changeAPIKey(String API_KEY*): void

> #### changeAPISecret(string API_SECRET*): void

> #### getX(): double 

> #### getY(): double

> #### getGeoFence(): string


* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact