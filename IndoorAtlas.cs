﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IndoorAtlas{

	static AndroidJavaObject plugin;
	static AndroidJavaClass jc;
	static AndroidJavaObject jo;

	// Starts the location services on the device. 
	// Returns true if the service was started successfully. 
	public static bool Initialise()
	{
		try {
			
			plugin = new AndroidJavaObject("edu.deakin.sp9.finallocationlibrary.UnityLocation");
			jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
		} catch (System.Exception ex) {
			Debug.Log (string.Format("CustomError: {0}", ex.Message));
		}

		if (jo == null) {
			Debug.Log ("Unable to initialise IndoorAtlas Class");
			return false; 
		} else {
			// IEnumerator function to call methods on the main thread
			jo.Call("runOnUiThread", new AndroidJavaRunnable(CallLocationFunctions));
			return true; 
		}
	}

	//Sets the context and starts location services. 
	public static void CallLocationFunctions()
	{
		try {
			plugin.Call ("setContext", jo);
			plugin.Call("startLocationServices");

		} catch (System.Exception ex) {
			Debug.Log(string.Format("CustomError Plugin: {0}", ex.Message));
		}
	}

	public static double GetX()
	{

		return plugin.Call<double> ("getX");
	}
	public static double GetY()
	{
		return plugin.Call<double> ("getY");
	}
	public static string GetGeoFence()
	{
		return plugin.Call<string> ("getGeofence");
	}
		


}
