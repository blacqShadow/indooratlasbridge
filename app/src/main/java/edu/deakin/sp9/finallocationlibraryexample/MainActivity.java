package edu.deakin.sp9.finallocationlibraryexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import edu.deakin.sp9.finallocationlibrary.UnityLocation;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UnityLocation unityLocation = new UnityLocation();
        unityLocation.testLibrary();
        unityLocation.setContext(this);
        unityLocation.startLocationServices();
    }
}
