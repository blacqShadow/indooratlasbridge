package edu.deakin.sp9.finallocationlibrary;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.indooratlas.android.sdk.IAGeofence;
import com.indooratlas.android.sdk.IAGeofenceEvent;
import com.indooratlas.android.sdk.IAGeofenceListener;
import com.indooratlas.android.sdk.IAGeofenceRequest;
import com.indooratlas.android.sdk.IALocation;
import com.indooratlas.android.sdk.IALocationListener;
import com.indooratlas.android.sdk.IALocationManager;
import com.indooratlas.android.sdk.IALocationRequest;
import com.indooratlas.android.sdk.IARegion;

import java.util.ArrayList;


public class UnityLocation {

    private IALocationManager iaLocationManager;
    private Context currentContext;
    private double x;
    private double y;
    private String sb;
    private String floorPlanId = "d4a5f6b9-5469-4dc8-b6da-418d9621e876";
    private String API_KEY = "1a4fe564-fc1f-4f02-94f6-2ca8e7879c92";
    private String API_SECRET = "NHxpkemSoxRLSQ5FZjR4zn787UTuhzxUQntV0B4zmyXJWxezHSpRb77HO6f6QL9DV8Z0gEFa/zCBqoTEkP05LipQ9xo7rXAwcAZpl4U9Za/2xudxYdxUP+JI/JhldA==";

    /******** GEOFENCING CODE *********************************************************************************/
    private boolean geoFencePlaced = false;

    private IAGeofenceListener iaGeofenceListener = new IAGeofenceListener() {
        @Override
        public void onGeofencesTriggered(IAGeofenceEvent iaGeofenceEvent) {
            // Assuming that we only have one geofence

            IAGeofence geofence = iaGeofenceEvent.getTriggeringGeofences().get(0);

            /*sb = "Geofence triggered. Geofence id: " + geofence.getId() + ". Trigger type: "
                    + ((iaGeofenceEvent.getGeofenceTransition() == IAGeofence.GEOFENCE_TRANSITION_ENTER) ?
            "ENTER": "EXIT");*/
            sb = ((iaGeofenceEvent.getGeofenceTransition() == IAGeofence.GEOFENCE_TRANSITION_ENTER) ?
                    "ENTER": "EXIT");

            Log.d("GEOFENCING: ", getGeofence());
            // Just to test the application, I will put in a hard coded string.
        }
    };

    // Get geofence data
    public String getGeofence() {
        if (sb == null)
            return "GeoFence Not Placed";
        else
            return sb;
    }
    /*********************************************************************************************************/

    // Gives out location updates
    private IALocationListener iaLocationListener = new IALocationListener() {
        @Override
        public void onLocationChanged(IALocation iaLocation) {

            // Log the received co-ordinates to the console.
            x = iaLocation.getLatitude();
            y = iaLocation.getLongitude();
            Log.d("LOCATION UPDATE", "LATITUDE: " + Double.toString(x));
            Log.d("LOCATION UPDATE", "LONGITUDE: " + Double.toString(y));

            // As soon as Location updates start, start geofencing aswell.
            Log.d("GEOFENCING: ", getGeofence());
            if(iaLocation.getAccuracy() < 10 && !geoFencePlaced)
            {
                //Place a geofence where you are after the location has converged to under 10 meter
                // accuracy
                placeNewGeofence(iaLocation);
                geoFencePlaced = true;
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }
    };

    // Function to check if the bridge is working as intended
    public void testLibrary()
    {
        Log.d("UnityLocation: ", "Function Called, Object Exists(Bridge Works)");
    }

    // Function to get app context from unity.
    public void setContext(Context context)
    {
        currentContext = context;
    }

    // Start location services
    public void startLocationServices()
    {
        Log.d("TESTING", "Start Location services function called");
        Log.d("MAIN", "Thread? " + Thread.currentThread());
        Bundle Keys = new Bundle();
        // Set the application keys found on the indoor atlas interface
        Keys.putString(iaLocationManager.EXTRA_API_KEY, API_KEY);
        Keys.putString(iaLocationManager.EXTRA_API_SECRET, API_SECRET);
        iaLocationManager = IALocationManager.create(currentContext, Keys);
        final IARegion region = IARegion.floorPlan(floorPlanId);
        iaLocationManager.setLocation(IALocation.from(region));
        iaLocationManager.requestLocationUpdates(IALocationRequest.create(),iaLocationListener);
    }

    // Pause the location services when the app is paused.
    public void pauseLocationServices()
    {
        iaLocationManager.removeLocationUpdates(iaLocationListener);
    }

    // Stop Location Services
    public void stopLocationServices()
    {
        iaLocationManager.destroy();
    }

    // Resume Location Services
    public void resumeLocationServices()
    {
        iaLocationManager.requestLocationUpdates(IALocationRequest.create(), iaLocationListener);
    }

    //Change the default floor plan id
    public void changeDefaultFloorPlanID(String newID)
    {
        floorPlanId = newID;
    }

    //S
    public void changeAPIKey(String api_key)
    {
        API_KEY = api_key;
    }
    public void changeAPISecret(String api_secret)
    {
        API_SECRET = api_secret;
    }


    // Get the X and Y coordinates
    public double getX()
    {
        return x;
    }
    public double getY() { return y; }

    // Placing a Rectangular Geofence
    /*************** RECTANGULAR GEOFENCE ***********************/
    private void placeNewGeofence(IALocation iaLocation)
    {
        ArrayList<double[]> edges = new ArrayList<>();
        // Approximate meter to coordinate transformations
        double latMeters = 0.4488 * 1e-4;
        double lonMeters = 0.4488 * 1e-4;
        // Size of the geofence 2 by 2 meter;
        double geofenceSize = 1;

        //Coordinates of the south-west corner of the geofence
        double lat1 = x - 0.5 * geofenceSize * latMeters;
        double lon1 = y - 0.5 * geofenceSize * lonMeters;

        //Coordinates of the north-east corner of the geofence
        double lat2 = x + 0.5 * geofenceSize * latMeters;
        double lon2 = y + 0.5 * geofenceSize * lonMeters;

        // Add them in clockwise order
        edges.add(new double[]{lat1, lon1});
        edges.add(new double[]{lat2 , lon1});
        edges.add(new double[]{lat2, lon2});
        edges.add(new double[]{lat1, lon2});

        IAGeofence geofence = new IAGeofence.Builder()
                .withEdges(edges)
                .withId("Region")
                .withTransitionType(IAGeofence.GEOFENCE_TRANSITION_ENTER|IAGeofence.GEOFENCE_TRANSITION_EXIT)
                .build();

        // This method will take the geofence listener
        iaLocationManager.addGeofences(new IAGeofenceRequest.Builder()
        .withGeofence(geofence).withInitialTrigger(IAGeofenceRequest.INITIAL_TRIGGER_ENTER).build(), iaGeofenceListener);
    }
    /*************************************************************/
}
